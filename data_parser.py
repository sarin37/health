#!/usr/bin/env python
import json
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import statsmodels.api as sm
from statsmodels.formula.api import ols

path_to_data = '/Users/sarin/Data/health/one-year-of-data.json'

data = []
for line in open(path_to_data, 'r+'):
    data.append(json.loads(line))

effective_time_frame = []
data_values = []
activity_names = []
for i in range(0, len(data)):
    x = data[i]['body'].keys()

    # extract time
    temp = []
    for j in x:
        if j == 'effective_time_frame':
            effective_time_frame.append([data[i]['body'][j]])
        else:
            temp.append(j)
            activity_names.append(j)

    # extract data
    temp2 = []
    for k in temp:
        temp2.append({'feature_name':k, 'feature_val':data[i]['body'][k]})
        # print temp2
    data_values.append(temp2)


data2 = zip(effective_time_frame, data_values)

bp_s = []
bp_d = []
bp_t = []

distance = []
distance_t = []
distance_dur = []

body_weight = []
body_weight_t = []

heart_rate = []
heart_rate_t = []

step_count = []
step_count_t = []
step_count_dur = []

activity_name = []
activity_name_t = []
activity_name_dur = []

minutes_moderate_activity = []
minutes_moderate_activity_t = []
minutes_moderate_activity_dur = []

for i in data2:
    # blood pressure: unit=mmHg
    if i[1][0]['feature_name'] == 'systolic_blood_pressure' or i[1][0]['feature_name'] == 'diastolic_blood_pressure':
        bp_s.append(i[1][0]['feature_val']['value'])
        bp_d.append(i[1][1]['feature_val']['value'])
        bp_t.append(datetime.strptime(i[0][0]['date_time'], '%Y-%m-%dT%H:%M:%SZ'))

    # body weight: unit=kg
    elif i[1][0]['feature_name'] == 'body_weight':
        body_weight.append(i[1][0]['feature_val']['value'])
        body_weight_t.append(datetime.strptime(i[0][0]['date_time'], '%Y-%m-%dT%H:%M:%SZ'))

    # heart rate: unit=beats/min
    elif i[1][0]['feature_name'] == 'heart_rate':
        heart_rate.append(i[1][0]['feature_val']['value'])
        heart_rate_t.append(datetime.strptime(i[0][0]['date_time'], '%Y-%m-%dT%H:%M:%SZ'))

    # step count: dur_unit=sec, step_count=count
    elif i[1][0]['feature_name'] == 'step_count':
        step_count.append(i[1][0]['feature_val'])
        step_count_t.append(datetime.strptime(i[0][0]['time_interval']['start_date_time'], '%Y-%m-%dT%H:%M:%SZ'))
        step_count_dur.append(i[0][0]['time_interval']['duration']['value'])

    # activity name (some activity): dur_unit=sec, distance=m
    elif i[1][0]['feature_name'] == 'distance':
        activity_name.append(i[1][0]['feature_val']['value'])
        activity_name_t.append(datetime.strptime(i[0][0]['time_interval']['start_date_time'], '%Y-%m-%dT%H:%M:%SZ'))
        activity_name_dur.append(i[0][0]['time_interval']['duration']['value'])

    # minutes moderate activity: dur_unit=min, minutes_moderate_activity=min
    elif i[1][0]['feature_name'] == 'minutes_moderate_activity':
        minutes_moderate_activity.append(i[1][0]['feature_val']['value'])
        minutes_moderate_activity_t.append(datetime.strptime(i[0][0]['time_interval']['start_date_time'], '%Y-%m-%dT%H:%M:%SZ'))
        minutes_moderate_activity_dur.append(i[0][0]['time_interval']['duration']['value'])

# build dataframe
bp_df = pd.DataFrame({'diastolic':bp_d, 'systolic':bp_s}, index=bp_t)
body_weight_df = pd.DataFrame({'body_weight':body_weight}, index=body_weight_t)
heart_rate_df = pd.DataFrame({'heart_rate':heart_rate}, index=heart_rate_t)
step_count_df = pd.DataFrame({'step_count':step_count, 'step_count_dur':step_count_dur}, index=step_count_t)
activity_name_df = pd.DataFrame({'activity_name':activity_name, 'activity_name_dur':activity_name_dur}, index=activity_name_t)
minutes_moderate_activity_df = pd.DataFrame({'minutes_moderate_activity':minutes_moderate_activity, 'minutes_moderate_activity_dur':minutes_moderate_activity_dur}, index=minutes_moderate_activity_t)

# resample
agg_period = '1D'

bp_df_agg = bp_df.resample(agg_period).apply(np.mean)
body_weight_df_agg = body_weight_df.resample(agg_period).apply(np.mean)
heart_rate_df_agg = heart_rate_df.resample(agg_period).apply(np.mean)
step_count_df_agg = step_count_df.resample(agg_period).apply(np.sum)
activity_name_df_agg = activity_name_df.resample(agg_period).apply(np.sum)
minutes_moderate_activity_df_agg = minutes_moderate_activity_df.resample(agg_period).apply(np.sum)

agg_data = pd.concat([bp_df_agg['diastolic'],
               bp_df_agg['systolic'],
               body_weight_df_agg['body_weight'],
               heart_rate_df_agg['heart_rate'],
               step_count_df_agg['step_count'],
               activity_name_df_agg['activity_name'],
               minutes_moderate_activity_df_agg['minutes_moderate_activity']],
              axis=1, keys=['diastolic', 'systolic', 'body_weight', 'heart_rate', 'step_count', 'activity_name', 'minutes_moderate_activity'])

# define which columns to compare
# cmp_columns = ['heart_rate', 'diastolic', 'systolic', 'body_weight', 'step_count', 'minutes_moderate_activity']
cmp_columns = ['diastolic', 'systolic', 'body_weight']

# find indexes of NaN in each column wanted
not_nan_idxs = []
for i in cmp_columns:
    not_nan_idxs.append(pd.notnull(agg_data[i]).nonzero()[0])

# dataframe of filtered by requested columns and nan
filt_agg_data = pd.concat([agg_data.iloc[list(sorted(set(not_nan_idxs[0]).intersection(*not_nan_idxs[1:])))][cmp_columns]], axis=1)

# print filt_agg_data.corr()


# model = ols("body_weight ~ systolic + diastolic + heart_rate + step_count + minutes_moderate_activity", data=filt_agg_data).fit()
model = ols("body_weight ~ systolic + diastolic", data=filt_agg_data).fit()
print model.params
print model.summary()

# ax = plt.gca()
# ax2 = ax.twinx()
# ax.plot(body_weight_df_agg.index, body_weight_df_agg, 'g')
# # ax.plot(step_count_df_agg.index, step_count_df_agg['step_count'], 'g')
# ax2.plot(heart_rate_df_agg.index, heart_rate_df_agg, 'r')
#
# plt.show()




# ax1 = plt.figure()
# plt.plot(step_count_df_agg.index, step_count_df_agg)
# ax2 = ax1.twinx()
# plt.plot(heart_rate_df_agg.index, heart_rate_df_agg)
# plt.show()


# plt.plot(bp_df.index)
# plt.plot(body_weight_df.index)
# plt.plot(heart_rate_df.index)
# plt.plot(step_count_df.index)
# plt.plot(activity_name_df.index)
# plt.plot(minutes_moderate_activity_df.index)

# plt.show()